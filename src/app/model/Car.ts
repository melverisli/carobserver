export class Car{
    private _id: string;
    get id(): string{
        return this._id;
    }
    set id(value :string){
        this._id = value
    }
    private _licenseplate: string;
    get licenseplate(): string{
        return this._licenseplate;
    }
    set licenseplate(value :string){
        this._licenseplate = value
    }
    private _finorvin: string;
    get finorvin(): string{
        return this._finorvin;
    }
    set finorvin(value :string){
        this._finorvin = value
    }
}