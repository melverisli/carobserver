import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Car } from '../model/Car';

@Injectable()
export class HeaderService {
    private _car: Car;
    private _api ="https://api.mercedes-benz.com/experimental/connectedvehicle/v1/vehicles";
    
    constructor(private _http : HttpClient) {
    }
    private _getHeader(): HttpHeaders{
        let header = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer 2622f78a-898d-48d9-ad65-347b38f4ec12'
          })
        return header;
    }
    public getCar(){
        return this._http.get(this._api, {headers:this._getHeader()});
    }
}
