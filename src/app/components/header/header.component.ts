import { Component, OnInit } from '@angular/core';
import { HeaderService } from 'src/app/services/header.service';

@Component({
  selector: 'header-comp',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers:[
     HeaderService
  ]
})
export class HeaderComponent implements OnInit {
    private _carId ="C1205C356AFA8F66EB";
    constructor(private _headerService:HeaderService){

    }

    ngOnInit(): void {
        this._getCar();
    }

    private _getCar(): void{
      this._headerService.getCar().subscribe(res=>{
        console.log("res",res);
      })
    }
}
